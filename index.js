const express = require('express');
const app = express();
const userRoute = require('./userRoute')


app.listen('3000', () => {
	console.log('server is running on 3000 port...')
})

app.use((req, res, next) => {
	console.log(`Request: ${req.url}`);
	next();
})

app.get('/', (req, res) => {
	res.send('Hello from Express App\n');
});

app.use('/api/user', userRoute);


app.use ((req, res, next) => {
	
	console.log(`Response: ${JSON.stringify(res.data)}`);
	next();	
})






