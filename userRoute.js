const express = require('express');
let app = express();

let userRouter = express.Router();

userRouter.route('/user1')
		  .get((req, res, next) => {
			  res.data = {userName : 'Sofikul', status : 'Active'};
			  res.send(res.data);
			  next();
		  })
		  
module.exports = userRouter;